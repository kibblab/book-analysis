#!/bin/bash

# remove (z-lib.org) from epub filenames

IFS=$'\n'
readarray -t files < <(ls *.epub --literal | sort -h) 

for ((i = 0; i < ${#files[@]}; ++i)); do
  NAME="$(basename ${files[$i]} .epub)"
  newname=$( echo $NAME | sed 's/ (z-lib.org)//g' )
  if [ ! -f $newname.epub ]; then
     mv -f ${files[$i]} $newname.epub
  fi
  pandoc --wrap=none -f epub -t plain $newname.epub -o $newname.txt &&
  sed -i 's/\[\]//g' $newname.txt # Remove empty []
  echo "$newname".txt
done
