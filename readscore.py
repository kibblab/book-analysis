from new_dale_chall_readability import cloze_score, reading_level as ndcrl
from readability import Readability
import json, re
import narrative

def score_all(filepath: str):
    results=dict()
    text=open(filepath).read()
    word_count=len(text.split())
    r = Readability(text)
    results['filename'] = filepath
    results['results'] = {}
    results['word_count'] = word_count

    dp=narrative.split(text, re.compile(r'[\"“]([^"]*?)[\"”]'))
    dp['narrative'] = list(filter(lambda n: n.strip(), dp['narrative']))
    dp['dialogue'] = list(filter(lambda n: n.strip(), dp['dialogue']))

    n_words = 0
    for n in dp['narrative']: n_words += len(n.split())
    d_words = 0
    for d in dp['dialogue']: d_words += len(d.split())

    results['dialogue_words_percent'] = round(d_words / (n_words + d_words), 4)
    
    d={
    'automated_readability_index': r.ari(),
    'coleman_liau': r.coleman_liau(),
    'flesch_kincaid': r.flesch_kincaid(),
    'flesch': r.flesch(),
    'linsear_write': r.linsear_write(),
    'smog': r.smog(all_sentences=True),
    'gunning_fog': r.gunning_fog(),
    'spache': r.spache()
    }

    for formula in d.keys():
        puthere = {}
        
        if hasattr(d[formula], 'score'):
            puthere.update({'score': d[formula].score}) 
        if hasattr(d[formula], 'grade_level'):
            
            try:
                g=int(d[formula].grade_level)
                puthere.update({'age': getMinimumAgeFromUsGrade(g)})
            except:
                g=d[formula].grade_level
            puthere.update({'grade_level': g})

        if hasattr(d[formula], 'grade_levels'):

            b=d[formula].grade_levels[0]

            if isinstance(b, list) and len(b) == 1:
                b=b[0]

            try:
                g=int(b)
                puthere.update({'age': getMinimumAgeFromUsGrade(g)})
            except:
                g=b
            puthere.update({'grade_level': g})
        
        results['results'][formula] = puthere

    results['results']['automated_readability_index'].update({'ages': d['automated_readability_index'].ages})
    results['results']['flesch'].update({'ease': d['flesch'].ease})
    results['results']['new_dale_chall'] = { 'cloze_score': cloze_score(text), 'grade_level' : ndcrl(text) }

    i=0
    avg=0
    for formula in d.keys():
        n=results['results'][formula]['grade_level']
        if isinstance(n, int):
            i+=1
            avg += n

    results['average_grade'] = avg / i
    print('%s\tDONE' % filepath)
    
    return results

def getMinimumAgeFromUsGrade(us_grade):
    """
    The age has a linear relation with the grade.
    http://en.wikipedia.org/wiki/Education_in_the_United_States#School_grades
    """
    if us_grade == 0:
        return 0

    from decimal import Decimal, ROUND_HALF_UP
    min_age = int(Decimal(us_grade + 5).quantize(Decimal('1'), rounding=ROUND_HALF_UP))
    if min_age < 0:
        return 0
    else:
        return min_age

from multiprocessing import Pool, cpu_count
import fire

def runtime(*inputfiles, output='results.json', cpucores=cpu_count()):
    """
    Readability Scorer

    Calculates dialogue % of book and readability scores.
    Formulas used:
      Automated Reading Index, Coleman-Liau index, Flesch-Kincaid, Flesch reading ease,
      Linsear Write, SMOG, Gunning fog, Spache, and New Dale-Chall
    
    :param output: Score results in JSON
    :param cpucores: Maximum number of CPU cores to process files. Default is your system maximum.
    """
    if cpucores > cpu_count():
        cpucores = cpu_count()

    print('Using %d CPU cores to run readability tests:' % cpucores)
    pool = Pool(cpucores)
    inputfiles=list(inputfiles) #glob.glob(inputfiles)
    out_put = open(output, "w")
    out_put.write('[')

    results=pool.map(score_all, [i for i in inputfiles ])
    results=sorted(results, key=lambda x: x['filename'])
    j=[ json.dumps(sc, indent=2) for sc in results ]
    print(*j, file=out_put, sep=',', end=']')
    pool.close()
    out_put.close()
    # print(inputfiles)
    # print(output)

if __name__ == '__main__':
    fire.Fire(runtime)


