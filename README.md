# Book and Text Analysis

## Requirements

- pandoc
- `pip install -r requirements.txt`

## Readability Scorer

<!-- https://github.com/prosegrinder/python-prosegrinder -->

Calculates dialogue % of book and readability scores.

Formulas used:
Automated Reading Index, Coleman-Liau index, Flesch-Kincaid, Flesch reading ease, Linsear Write, SMOG, Gunning fog, Spache, and New Dale-Chall.

```
git clone git@github.com:wimmuskee/readability-score.git
pip install ./readability-score
```

### Instructions

1. Convert epub to plaintext using pandoc.

    ```bash
    pandoc --wrap=none -f epub -t plain -o aaa.txt aaa.epub
    ```

    For multiple:

    ```bash
    for i in *.epub ; do echo "$i" && pandoc --wrap=none -f epub -t plain "$i" -o "$i".txt ; done
    ```

2. Run `python3 readscore.py --help`

    ```
    NAME
        readscore.py - Readability Scorer

    SYNOPSIS
        readscore.py <flags> [INPUTFILES]...

    POSITIONAL ARGUMENTS
        INPUTFILES

    FLAGS
        --output=OUTPUTFILE
            Default: 'results.json'
            Score results in JSON
        --cpucores=CPUCORES
            Maximum number of CPU cores to process files. Default is your system maximum.
    ```

3. Get readability scores.

    ```bash
    python3 readscore.py *.txt --output=results.json
    ```

<!-- ## Analysis

```bash
prosegrinder $dir/*.md -s $dir/results.json
``` -->


<!-- ## Short Story magazine

1. Go through the `.md` and cleanup. Use `---` to separate stories, and label each story with `# "Title" by Author`

2. Split stories into their own file:

```bash
dir=issue53
mkdir $dir
csplit --elide-empty-files --keep-files --suppress-matched --prefix="$dir/" -b %02d.md neon53.md /---/ '{*}'
``` 

4. Get title of Markdown files
`jq -R 'scan("^#\\s*\"(?<a>.*)\".*$") | .[]' issue53/*.md`
-->


## NLP


```
python3 -m nltk.downloader punkt
```

```
python3 -m spacy download en_core_web_sm en_core_web_lg 
```

Uses [Syntactic dependency relation labels](https://github.com/clir/clearnlp-guidelines/blob/master/md/specifications/dependency_labels.md) to identify subject-verb-object (SVO). For Spacey, click on [Label Scheme - Parser](https://spacy.io/models/en#en_core_web_lg) for all the parts-of-speech labels that can be used.


## References

- [Cloze score](https://en.wikipedia.org/wiki/Cloze_test)
- [Dale Chall Easy English Words List](https://github.com/words/dale-chall)